<?php
use \PHPUnit\Framework\TestCase;

class ArrayHelperTest extends TestCase
{
    public function testeArray_diff_assoc_recursive(): void
    {
        $this->assertEquals(
            [1 => 'b', 2 => 2],
            Helpers\ArrayHelper::array_diff_assoc_recursive(['a', 'b', '2'], ['b', 0=>'a', '2'])
        );
    }
}