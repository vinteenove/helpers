<?php


use Helpers\JsonHelper;
use PHPUnit\Framework\TestCase;

class JsonHelperTest extends TestCase
{

    public function testEncode()
    {
        $this->assertIsString(
            Helpers\JsonHelper::encode(['teste' => 'teste2'])
        );
    }
}
