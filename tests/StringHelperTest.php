<?php


use Helpers\StringHelper;
use PHPUnit\Framework\TestCase;

class StringHelperTest extends TestCase
{

    public function testFormatDir()
    {
        $this->assertEquals(
            'cao',
            Helpers\StringHelper::formatDir('ÇãÓ')
        );
    }

    public function testIsJson()
    {
        $this->assertEquals(
            true,
            Helpers\StringHelper::isJson('{"teste": "teste"}')
        );
    }

    public function testRun()
    {
        $this->assertIsString(
            Helpers\StringHelper::run()
        );
    }

    public function testStripTagsContent()
    {
        $this->assertEquals(
            'boi',
            Helpers\StringHelper::stripTagsContent('<b>oi</b>boi')
        );
    }

    public function testCheckAllowedWords()
    {
        $this->assertEquals(
            true,
            Helpers\StringHelper::checkAllowedWords('boi', ['boi'])
        );
    }

    public function testStartsWith()
    {
        $this->assertEquals(
            true,
            Helpers\StringHelper::startsWith('boi', 'b')
        );
    }

    public function testMask()
    {
        $this->assertEquals(
            '12-34-5',
            Helpers\StringHelper::mask('12345', '##-##-#')
        );
    }

    public function testFormatURL()
    {
        $this->assertEquals(
            'boi-cao',
            Helpers\StringHelper::formatURL(' bói ção ')
        );
    }


    public function testSlug()
    {
        $this->assertEquals(
            'boi-cao',
            Helpers\StringHelper::slug(' bói ção ')
        );
    }

    public function testEndsWith()
    {
        $this->assertEquals(
            true,
            Helpers\StringHelper::endsWith('boi', 'i')
        );
    }


    public function testRandom()
    {
        $this->assertEquals(
            3,
            strlen(Helpers\StringHelper::random(3))
        );
    }

    public function testTranslate()
    {
        $this->assertEquals(
            'teste',
            Helpers\StringHelper::translate(':test', ['test' => 'teste'])
        );
    }

    public function testCheckMaxLenght()
    {
        $this->assertEquals(
            true,
            Helpers\StringHelper::checkAllowedWords('test', ['test', 'teste'])
        );
    }
}
