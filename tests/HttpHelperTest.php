<?php
require_once 'vendor/autoload.php';

use \PHPUnit\Framework\TestCase;

class HttpHelperTest extends TestCase
{
    public function testeBuildQuery(): void
    {
        $this->assertIsString(
            Helpers\HttpHelper::buildQuery(['teste'])
        );
    }

    public function testeBuildFriendlyUri(): void
    {
        $_SERVER['HTTP_HOST'] = $_SERVER['REQUEST_URI'] = 'test';

        $this->assertIsString(
            Helpers\HttpHelper::buildFriendlyUri('test')
        );
    }

    public function testePathToUri(): void
    {
        $this->assertIsString(
            Helpers\HttpHelper::pathToUri(__DIR__)
        );
    }

    public function testePath(): void
    {
        $this->assertIsString(
            Helpers\HttpHelper::path('test')
        );
    }

    public function testeParse_get(): void
    {
        $this->assertIsArray(
            Helpers\HttpHelper::parse_get(['test'])
        );
    }

    public function testeParse_friendly(): void
    {
        $this->assertIsArray(
            Helpers\HttpHelper::parse_friendly('test')
        );
    }

    public function testeGetPageUri(): void
    {

        $this->assertIsString(Helpers\HttpHelper::getPageUri());
    }

    public function testeParseFriendlyUri(): void
    {

        $this->assertIsArray(
            Helpers\HttpHelper::parseFriendlyUri('teste')
        );

    }

    public function testeBasicUrlAnalyzer(): void
    {
        $this->assertIsArray(
            Helpers\HttpHelper::basicUrlAnalyzer('teste')
        );
    }
}