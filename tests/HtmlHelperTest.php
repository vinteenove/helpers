<?php


use Helpers\HtmlHelper;
use PHPUnit\Framework\TestCase;

class HtmlHelperTest extends TestCase
{

    public function testCss()
    {
        $this->assertIsString(
            Helpers\HtmlHelper::css(['teste' => 'teste'])
        );
    }

    public function testAttr()
    {
        $this->assertIsString(
            Helpers\HtmlHelper::attr(['teste' => 'teste'])
        );
    }

    public function testStyle()
    {
        $this->assertIsString(
            Helpers\HtmlHelper::style(['teste' => 'teste'])
        );
    }

    public function testFormatByte()
    {
        $this->assertIsString(
            Helpers\HtmlHelper::formatByte(1.1)
        );
    }

    public function testClass()
    {
        $this->assertIsString(
            Helpers\HtmlHelper::class('teste', 'teste2', 'teste3')
        );
    }

    public function testIssetValue()
    {
        $ponteiro = ['teste2', 'teste' => 'teste3'];
        $this->assertIsBool(
            Helpers\HtmlHelper::issetValue('teste', $ponteiro)
        );
    }

    public function testValue()
    {
        $this->assertIsString(
            Helpers\HtmlHelper::value('teste', ['teste' => 'teste2'])
        );
    }

    public function testSelect()
    {
        if (!class_exists("ShortTag")) {
            $this->markTestSkipped();
        }

        $this->assertIsString(
            Helpers\HtmlHelper::select('teste', ['teste' => 'teste2'], ['teste3', 'teste4'])
        );
    }

    public function testInput()
    {
        $this->assertIsString(
            Helpers\HtmlHelper::input('teste', ['teste' => 'teste2'])
        );
    }
}
