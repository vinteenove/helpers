<?php

namespace Helpers;


class JsonHelper
{
    /**
     * json_encode alternativo com opção de compactar
     *
     * @param $array
     * @param bool $compacto
     * @return string
     */
    static public function encode($array, $compacto = true){
        return self::arrayToJsonRecursive($array, $compacto);
    }

    /**
     * @param $array array parar convercao
     * @param bool $c [true: Compacto, false: descompactado (identado)]
     * @param string $t Tabulação
     * @return string string json
     */
    private static function arrayToJsonRecursive($array, $c = false, $t = ''){
        $aS = array(); $oS = array(); $tA = true; $kR = 0;
        foreach ($array as $kA => $v){
            $k = self::escapeJsonString($kA);
            switch (gettype($v)) {
                default;
                case "object":
                    $v = (array)$v;
                case "array":
                    $oS[] = '"' . $k . '":' . (!$c ? ' ' : '') . ($aS[] = (self::arrayToJsonRecursive($v, $c, ("\t" . $t))));
                    break;
                case "boolean":
                    $oS[] = '"' . $k . '":' . (!$c ? ' ' : '') . ($aS[] = ($v ? 'true' : 'false'));
                    break;
                case "integer":
                case "double":
                    $oS[] = '"' . $k . '":' . (!$c ? ' ' : '') . ($aS[] = ($v));
                    break;
                case "string":
                    $oS[] = '"' . $k . '":' . (!$c ? ' ' : '') . ($aS[] = ('"' . self::escapeJsonString($v) . '"'));
                    break;
                case "NULL":
                    $oS[] = '"' . $k . '":' . (!$c ? ' ' : '') . ($aS[] = ('null'));
            }
            $tA = (is_numeric($kA) && $kA == $kR ? $tA : false);
            $kR++;
        } return (empty($aS)? '{}': (!$tA? "{": "["). (!$c? "\n\t". $t: '') . implode(",". (!$c? "\n\t". $t: ''), (!$tA? $oS: $aS)). (!$c? "\n". $t: ''). (!$tA? "}": "]"));
    }

    /**
     * @param string $value texto para tratar
     * @return string texto tratado
     */
    private static function escapeJsonString($value){
        return str_replace(
            array(  "\\",   "\"",   "/",  "\n",  "\r",  "\t", "\x08", "\x0c"),
            array("\\\\", "\\\"", "\\/", "\\n", "\\r", "\\t", "\\f",  "\\b" ),
            $value
        );
    }
}