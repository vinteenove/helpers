<?php

namespace Helpers;

/**
 * Class StringHelper
 *
 * @category Helpers
 * @package  Helpers
 * @author   Jeison Frasson
 * @author   Rodrigo Dechen <dechen.developer@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 */
class ArrayHelper
{
	public static function array_diff_assoc_recursive($array1, $array2)
	{
		foreach($array1 as $key => $value){
			if(is_array($value)){
				if(!isset($array2[$key])){
					$difference[$key] = $value;
				}elseif(!is_array($array2[$key])){
					$difference[$key] = $value;
				}else{
					$new_diff = self::array_diff_assoc_recursive($value, $array2[$key]);
					if($new_diff != FALSE){
						$difference[$key] = $new_diff;
					}
				}
			}elseif(!array_key_exists($key, $array2) || $array2[$key] != $value){
				$difference[$key] = $value;
			}
		}
		return !isset($difference)? null: $difference;
	}
	
	public static function calculate_changes(array &$old, array &$new, array $key = ['id']): array
	{
		$in = self::udiff_ref($new, $old, fn($a, $b) => self::compare_keys($a, $b, $key));
		$de = self::udiff_ref($old, $new, fn($a, $b) => self::compare_keys($a, $b, $key));
		
		$nw = self::udiff_ref($new, $in, fn($a, $b) => self::compare_keys($a, $b, $key));
		$od = self::udiff_ref($old, $de, fn($a, $b) => self::compare_keys($a, $b, $key));
		
		$up = [];
		foreach($od as $o){
			foreach($nw as &$n){
				if(self::compare_keys($o, $n, $key) !== 0){
					continue;
				}
				
				$up[] =& self::merge_ref($n, $o);
			}
		}
		
		return [$in, $up, $de];
	}
	
	public static function get_key($data, array $key = ['id']): array
	{
		$target = [];
		foreach($key as $k){
			if(is_array($data) && array_key_exists($k, $data)){
				$target[$k] = $data[$k] ?? null;
			}elseif(is_object($data) && property_exists($data, $k)){
				$target[$k] = $data->$k ?? null;
			}
		}
		return $target;
	}
	
	public static function udiff_ref(array &$array1, array &$array2, callable $callback): array
	{
		$result = [];
		foreach($array1 as $key1 => &$value1){
			$unique = true;
			foreach($array2 as $value2){
				if($callback($value1, $value2) === 0){
					$unique = false;
					break;
				}
			}
			if($unique){
				$result[$key1] =& $value1;
			}
		}
		return $result;
	}
	
	public static function compare_keys($a, $b, array $key): int
	{
		return self::get_key($a, $key) <=> self::get_key($b, $key);
	}
	
	public static function &merge_ref(array &$array1, array $array2): array
	{
		foreach($array2 as $k => &$v){
			if(!array_key_exists($k, $array1)){
				$array1[$k] =& $v;
			}
		}
		
		return $array1;
	}
	
}
