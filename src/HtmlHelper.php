<?php

namespace Helpers;

use Exception;
use ShortTag\ShortTag;

class HtmlHelper{

    /**
     * @param $name
     * @param $dados
     * @param array $options
     * @param false $first
     * @return string
     */
    public static function select(string $name, $dados, array $options, $first = false)
    {
        $r = '';

        try{
            if(isset($dados[$name])) {
                $value = $dados[$name];
            }else{
                $value = self::value($name, $dados);
            }
        }catch(Exception $e){
            $value = null;
        }

        foreach ($options as $val => $opt){
            $data = ShortTag::Search($val);
            $data = (($data && isset($data[0]))? $data[0]: []);
            $key = (($data)? substr($val, 0, $data['start']): $val);

            $attr = array();
            if(!empty($data)) {
                foreach ($data['shortTag'] as $k => $v) {
                    $attr[((is_numeric($k)) ? $v : $k)] = $v;
                }
            }

            if (is_array($opt)) {
                $attr = self::attr(array_replace(array(
                    'label' => $key
                ), $attr));
                $r .= "<optgroup{$attr}>" . self::select($name, $dados, $opt, $first) . '</optgroup>';
            } else {
                $attr = self::attr(array_replace(['value' => $key], ((($first !== null && $first != true && $first = true) || ((is_array($value) && array_search($key, $value) !== false) || (!is_null($value) && ((is_string($value) && $value === ((string) $key)) || (is_numeric($value) && ((float) $value) === ((float) $key))))))? ['selected' => 'selected']: []), $attr));
                $r .= "<option{$attr}>{$opt}</option>";
            }
        }
        return $r;
    }

    /**
     * monta os parametros name, valeu e checked de um input
     *
     * $name, pode ser array ou string.
     * Como string procura em $dados por $nome e é atribuido a tag name.
     * Como array ['name', 'search']. Na chave 0 é o nome atribuido a tag e a chave 1 é usada para procurar em dados.
     *
     * $dados é um array com os dados para o formulario.
     *
     * $chackbox, se false não intende como checkbox e, se string compara se é igual, o value que o nome refencia com valor de $chackbox.
     *
     * @param array|string $name
     * @param array        $dados
     * @param bool|string  $chackbox
     * @return string
     */
    public static function input($name, array $dados, $chackbox = false): string
    {
        $search = ((is_array($name) && isset($name[1]))? $name[1]: (string) $name);
        $name = ((is_array($name) && isset($name[0]))? $name[0]: (string) $name);
        try{
            if(isset($dados[$search])) $value = $dados[$search];
            else $value = self::value($search, $dados);
        } catch(Exception $e){
            $value = null;
        }

        $attr = [];
        if($name !== false){
            $attr['name'] = $name;
        }

        if($chackbox !== false){
            $attr['value'] = (is_null($chackbox) ? 'null' : (string) $chackbox);
            if((is_array($value) && array_search($chackbox, $value) !== false) || $value == $chackbox) $attr['checked'] = 'checked';

        } elseif(!is_null($value) && !is_array($value)) {
            $attr['value'] = (string)$value;
        }

        return self::attr($attr);
    }

    /**
     * coloque um array e transforma em atributos de uma teg html
     * @param array $attrs
     * @return string
     */
    public static function attr(array $attrs): string
    {
        $r = '';
        foreach($attrs as $k => $v){
            switch($k){
                case 'class':
                    $attrs[$k] = self::class($v);
                break;
                case 'style':
                    $attrs[$k] = ((is_array($v))? self::style($v): (string) $v);
                break;
                default;
                    if($v === false){
                        unset($attrs[$k]);
                    }elseif($v === true){
                        $attrs[$k] = $k;
                    }elseif(is_array($v) || is_object($v)){
                        $attrs[$k] = htmlspecialchars(json_encode($v));
                    }elseif(is_null($v)){
                        $attrs[$k] = '';
                    }
                break;
            }
        }
        foreach($attrs as $k => $v){
            $r .= " {$k}=\"{$v}\"";
        }
        return $r;
    }

    /**
     * @param ...$class
     * @return string
     */
    public static function class(...$class): string
    {
        $buff = [];
        foreach($class as $cls){
            $add = $del = [];
            foreach(array_filter(explode(' ', $cls)) as $item){
                if($item[0] === '!'){
                    $del[] = substr($item, 1);
                }else{
                    $add[] = $item;
                }
            }
            $buff = array_merge($buff, $add);
            array_unique($buff);
            $buff = array_diff($buff, $del);
        }
        return implode(' ', $buff);
    }

    /** Compial um array em um css para ser usaro em uma atrubuto css de uma tag
     * @param array $css
     * @return string
     */
    public static function style(array $css): string
    {
        $r = [];
        foreach($css as $k => $v){
            switch(gettype($v)){
                case 'integer': case 'double':
                    $r[] = $k . ': ' . $v . 'px';
                break;
                case 'array':
                    $r[] = $k . ': ' . implode(', ', $v);
                break;
                default:
                    $r[] = $k . ': ' . $v;
                break;
            }
        }
        return htmlspecialchars(implode('; ', $r));
    }

    /** Compila css para ser usado em arquivos css's
     * @param array $css
     * @return string
     */
    public static function css(array $css)
    {
        $r = '';
        foreach($css as $k => $v){
            $r .= "{$k}{";
            if(is_array($v)) foreach($v as $prop => $val)
                $r .= "{$prop}: {$val};";
            else
                $r .= $v;
            $r .= "}";

        }return $r;
    }

    /** Retorna o $value que a $key esta associada
     * @param string $key
     * @param mixed $dados
     * @param mixed $defalt
     * @return mixed
     */
    public static function value(string $key, $dados, $defalt = null)
    {
        parse_str("{$key}=", $aKey);
        return self::valueRecursive($aKey, $dados, $defalt);
    }

    /** Auxilia montaValue a achar o valor procurado
     * @param mixed $aKey
     * @param mixed $dados
     * @param mixed $defalt
     * @return mixed
     */
    private static function valueRecursive($aKey, $dados, $defalt = null)
    {
        if(!is_array($aKey)) return $dados;
        foreach($aKey as $k => $v) break;
        if(!isset($k, $dados[$k])) return $defalt;
        return self::valueRecursive($aKey[$k], $dados[$k]);
    }

    /** Retorna true caso exista a chave e false se não existir
     * @param string $key
     * @param array  $dados
     * @return bool
     */
    public static function issetValue(string $key, array &$dados): bool
    {

        parse_str("{$key}=", $aKey);

        return self::issetValueRecursive($aKey, $dados);
    }

    /**
     * @param $aKey
     * @param $dados
     * @return bool
     */
    private static function issetValueRecursive($aKey, &$dados): bool
    {
        if(!is_array($aKey)) {
            return true;
        }

        foreach($aKey as $k => $v) {
            break;
        }

        if(!isset($dados[$k])){
            return false;
        }

        return self::issetValueRecursive($aKey[$k], $dados[$k]);
    }

    /**
     * Formata o numero e coloca un sufixo referente ao tameno em bytes
     * @param float  $number
     * @param int    $decimals
     * @param string $dec_point
     * @param string $thousands_sep
     * @return string
     */
    public static function formatByte(float $number , $decimals = 0 , $dec_point = '.' , $thousands_sep = ','): string
    {
        $lessThan = null;

        foreach([
            'Y' => 80,
            'Z' => 70,
            'E' => 60,
            'P' => 50,
            'T' => 40,
            'G' => 30,
            'M' => 20,
            'k' => 10,
            '' => 0,
            'm' => -10,
            'µ' => -20,
            'n' => -30,
            'p' => -40,
            'f' => -50,
            'a' => -60,
            'z' => -70,
            'y' => -80,
        ] as $s => $e) {
            $biggerThen = (2 ** $e);
            if($number < $lessThan && $number >= $biggerThen){
                return number_format(($number / $biggerThen), $decimals, $dec_point, $thousands_sep) . $s;
            }
            $lessThan = $biggerThen;
        }
        return number_format(($number / $lessThan), $decimals, $dec_point, $thousands_sep) . $s;
    }

}