<?php

namespace Helpers;

/**
 * Class StringHelper
 *
 * @category Helpers
 * @package  Helpers
 * @author   Andre Ribas
 * @license  MIT https://opensource.org/licenses/MIT
 */
class StringHelper
{
    /**
     * Run
     *
     * Function just for test
     *
     * @return string
     */
    public static function run()
    {
        return 'string helper running!';
    }

    /**
     * Slug
     *
     * Convert text into slug
     *
     * @param String $text Text to create a slug
     *
     * @return mixed|string
     */
    public static function slug($text)
    {
        if (is_string($text)) {

            $text = strtolower(trim(utf8_decode($text)));

            $before  = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑ';
            $before .= 'ÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæç';
            $before .= 'èéêëìíîïðñòóôõöøùúûýýþÿRr';

            $after   = 'aaaaaaaceeeeiiiidn';
            $after  .= 'oooooouuuuybsaaaaaaac';
            $after  .= 'eeeeiiiidnoooooouuuyybyRr';

            $text   = strtr($text, utf8_decode($before), $after);

            $replace = array(
                '/[^a-z0-9.-]/' => '-',
                '/-+/'          => '-',
                '/\-{2,}/'      => ''
            );

            $text = preg_replace(
                array_keys($replace),
                array_values($replace),
                $text
            );
        }
        return $text;
    }

    /**
     * Generates a random string.
     *
     * @param int $len
     * @param string $pool
     *
     * @return string
     */
    public static function random(
        $len,
        $pool = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'
    ) {
        return substr(str_shuffle(str_repeat($pool, $len)), 0, $len);
    }

    /**
     * Função para criar máscaras dinamicamente
     *
     * @param $val
     * @param $mask
     * @return string
     */
    public static function mask($val, $mask)
    {
        $maskared = '';
        $k = 0;
        if( !empty($val) ){
            for($i = 0; $i<=strlen($mask)-1; $i++)
            {
                if($mask[$i] == '#')
                {
                    if(isset($val[$k]))
                        $maskared .= $val[$k++];
                }
                else
                {
                    if(isset($mask[$i]))
                        $maskared .= $mask[$i];
                }
            }
            return $maskared;
        } else{
            return $val;
        }

    }

    /**
     * Função para formatar URL
     *
     * @param $texto
     * @return mixed
     */
    public static function formatURL($texto)
    {
        $texto = trim($texto);
        $texto = static::formatDir($texto);
        $texto = preg_replace("/[^ a-z 0-9 \t _ \/ -]/", "", $texto);
        $texto = str_replace(" ","-",$texto);
        return($texto);
    }

    /**
     * Função para formatar caracteres especiais
     *
     * @param string $texto
     * @return string
     */
    public static function formatDir(string $texto): string
    {
        $texto = mb_strtolower($texto); // muda tudo para minusculo
        $texto = str_replace("ç","c",$texto);
        $texto = str_replace(['á','à','â','ã','ª'],"a",$texto);
        $texto = str_replace(['é','è','ê'],"e",$texto);
        $texto = str_replace(['í','ì','î','ï'],"i",$texto);
        $texto = str_replace(['ó','ò','ô','õ','º'],"o",$texto);
        $texto = str_replace(['ú','ù','û'],"u",$texto);
        $texto = preg_replace("/[^ a-z 0-9 \t\n _ -- . \\ \/]/", "", $texto);
        return($texto);
    }

    /**
     * Traduz os valores de uma string.
     * Ex:
     * $str = 'Meu nome é :nome';
     * echo StringHelper::translate($str, ['nome' => 'João']);
     * //Saída: Meu nome é João
     *
     * @param string $str
     * @param array  $table
     *
     * @return string
     */
    public static function translate($str, array $table)
    {
        foreach ($table as $key => $value) {
            $table[":$key"] = $value;
            unset($table[$key]);
        }

        return strtr($str, $table);
    }

    /**
     * VERIFICA SE O PARAMETRO EH NUMERICO INTEIRO
     *
     * @param string $str
     *
     * @return string
     *
     * @deprecated
     */
    public static function isNumbers($str)
    {
        trigger_error("Utilize a função 'is_integer()' ou 'is_numeric()' para verificar numeros inteiros.", E_USER_WARNING);

        // if (preg_replace("/([0-9])\w+/", "", $texto)) {
        // if ( preg_match( '/[^0-9A-Za-z\,\.\-\_\s ]/is', $variavel ) ) {
        if (preg_match('/[^0-9]/is', $str)) {
            return true;
        }else{
            return false;
        }
    }

    /**
     * @param string $str
     * @return bool
     * @deprecated
     */
    public static function isFloatNumbers($str)
    {
        trigger_error("Utilize a função 'is_float()' ou 'is_numeric()' para verificar numeros inteiros.", E_USER_WARNING);

        if (preg_match('/[^0-9\.\ ]/is', $str)) {
            return true;
        }else{
            return false;
        }
    }

    /**
     * @param string $str
     * @param int $max_length
     * @return bool
     */
    public static function checkMaxLenght($str,$max_length)
    {
        //@TODO: Logica invertida, corrigir.
        return strlen($str) > $max_length;
    }

    /**
     * @param $str
     * @param array $allowed_words
     * @return bool
     */
    public static function checkAllowedWords($str, array $allowed_words)
    {
        return in_array($str, $allowed_words);
    }
 
    /**
     * Verifica se se é uma data valida no formato MM-DD-YY
     *
     * @param string $str - DATA A SER VALIDADA
     *
     * @return return['status'], e se houver erro: return['dia'] ou return['mes'] ou return['ano']
     *
     * @deprecated
     */
    public static function checkDate($str)
    {
        trigger_error("Utilize a classe DateTime para fazer validação de datas.", E_USER_WARNING);

        if( strpos( $str, '-' ) !== false ){

            $erro = array();
            $data = explode('-', $str);

            if ($data[0] > '12') {
                $erro['mes'] = 'mes inexistente';
            }else{
                if($data[0] == '01' || $data[0] == '03' || $data[0] == '05' || $data[0] == '07' || $data[0] == '08' || $data[0] == '10' || $data[0] == '12' ){
                    $max_dia = '31';
                }else{
                    if ($data[0] == '02' && $data[2] % 4 == 0) {
                        $max_dia = '29';
                    }elseif($data[0] == '02'){
                        $max_dia = '28';
                    }else{
                        $max_dia = '30';
                    }
                }
            }
            if($data[1]>$max_dia){
                $erro['dia'] = 'dia maior do que o abrangido pelo mes: '.$data[0];
            }
            if ($data[2]<date('y')) {
                $erro['ano'] = 'ano menor do que o ano atual';
            }

        }else{
            $erro['formato'] = 'data invalida';
        }


        if (!empty($erro)) {
           $erro['status'] = false;
           return $erro;
        }else{
           $erro['status'] = true;
           return $erro;
        }
        
        // if (preg_match("/(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])-(9[0-9]|0[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9])/",$str))
     
    }

    /**
     * @deprecated
     */
    public static function checaValor($valor)
    {
        trigger_error("Função depreciada. Utilize 'empty()'.", E_USER_WARNING);

        if (!empty($valor) && $valor != '' && $valor != ' ' && $valor != null && $valor != NULL && $valor != 'null' && $valor != 'NULL' ) {
            return true;
        }else{
            return false;
        }
    }

    /**
     * Checks if $haystack starts with $needle.
     *
     * @param string $haystack
     * @param string $needle
     * @param bool $sensitive Optional: differs capital characters? Default: true
     *
     * @return bool
     */
    public static function startsWith($haystack, $needle, $sensitive = true)
    {
        if (!$sensitive) {
            return mb_stripos($haystack, $needle) === 0;
        }

        return mb_strpos($haystack, $needle) === 0;
    }

    /**
     * Checks if $haystack ends with $needle.
     *
     * @param string $haystack
     * @param string $needle
     * @param bool $sensitive Optional: differs capital characters? Default: true
     *
     * @return bool
     *
     * @see https://github.com/Corviz/framework/blob/master/src/String/StringUtils.php (endsWith method)
     */
    public static function endsWith($haystack, $needle, $sensitive = true)
    {
        $needleLength = mb_strlen($needle);
        $cmp = substr_compare(
            $haystack,
            $needle,
            -($needleLength),
            $needleLength,
            !$sensitive
        );

        return $cmp === 0;
    }

    /**
     * Checks if given string content is a JSON.
     *
     * @param $str
     *
     * @return bool
     *
     * @see https://stackoverflow.com/questions/6041741/fastest-way-to-check-if-a-string-is-json-in-php/6041773#6041773
     */
    public static function isJson($str)
    {
        if (!is_string($str)) {
            return false;
        }

        json_decode($str);
        return (json_last_error() == JSON_ERROR_NONE);
    }

    /**
     * Strip tags along with their content
     *
     * @param $text
     * @param string $tags
     * @param bool $invert When true, will remove only specified instead of keep them.
     *
     * @return string
     *
     * @see http://php.net/manual/en/function.strip-tags.php#86964
     */
    public static function stripTagsContent($text, $tags = '', $invert = false)
    {

        preg_match_all('/<(.+?)[\s]*\/?[\s]*>/si', trim($tags), $tags);
        $tags = array_unique($tags[1]);

        if(is_array($tags) && count($tags) > 0) {
            if($invert == false) {
                return preg_replace('@<(?!(?:'. implode('|', $tags) .')\b)(\w+)\b.*?>.*?</\1>@si', '', $text);
            }
            else {
                return preg_replace('@<('. implode('|', $tags) .')\b.*?>.*?</\1>@si', '', $text);
            }
        }
        elseif($invert == false) {
            return preg_replace('@<(\w+)\b.*?>.*?</\1>@si', '', $text);
        }
        return $text;
    }
}
