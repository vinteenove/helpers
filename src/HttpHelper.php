<?php

namespace Helpers;

use function parse_url;

class HttpHelper
{
	
	/**
	 * @param array  $query_data
	 * @param string $arg_separator
	 * @return string
	 */
	public static function buildQuery(array $query_data, string $arg_separator = '/'): string
	{
		@array_walk_recursive($query_data, function(&$v, $k){
			$v = rawurlencode($v);
		});
		return rawurldecode(self::buildQueryRecursive($query_data, $arg_separator));
	}
	
	private static function buildQueryRecursive(array $query_data, string $arg_separator = '/', array $prefix = [], string &$query = ''): string
	{
		foreach($query_data as $k => $data){
			if(is_array($data)){
				self::buildQueryRecursive($data, $arg_separator, array_merge($prefix, [$k]), $query);
			}else{
				$key = self::buildNameKey(array_merge($prefix, [$k]));
				$key .= ((empty($key))? '': '=');
				$data = (string) $data;
				$query .= ((empty($query))? '': $arg_separator) . "{$key}{$data}";
			}
		}
		return ((empty($query))? '': $query . $arg_separator);
	}
	
	private static function buildNameKey(array $arrayName): string
	{
		@array_walk($arrayName, function(&$v){
			$v = ((is_numeric($v))? '': $v);
		});
		$first = array_shift($arrayName);
		$name = implode('][', $arrayName);
		$name = ((!empty($name))? "[{$name}]": '');
		return "{$first}{$name}";
	}
	
	/**
	 * @param string $uri
	 * @param array  $query
	 * @param array  $exclude
	 * @return string
	 */
	public static function buildFriendlyUri(string $uri, array $query = [], array $exclude = [])
	{
		$url = self::parse_friendly($uri);
		$url = array_merge($url, $query);
		$url = array_diff_key($url, array_flip($exclude));
		return self::buildQuery($url);
	}
	
	/**
	 * Resolve um url. Incluindo o $get e removendo as chaves en $exclude.
	 * Se a url tiver host, so procesa os gets. Se nao tiver retorna uma url em relacao ao host atual.
	 *
	 * @param string $path          Caminho completo do arquivo que deve ser gerada a url
	 * @param array  $get           Array com a query para a url, isto é, tudo depois da ?(interogacao)
	 * @param array  $exclude       Chaves que serao excluidas da query
	 * @param string $arg_query     Caracter que separata o path da query
	 * @param string $arg_separator Caracter que separa os itens da queru
	 * @return string               retorna  string com a url processada
	 */
	public static function pathToUri(string $path, array $get = [], array $exclude = [], string $arg_query = '?', string $arg_separator = '&'): string
	{
		$path = self::path($path, '/');
		
		$f = parse_url($path);
		$b = parse_url(dirname(str_replace('//', '/', $_SERVER['SCRIPT_FILENAME'])));
		
		if(isset($f['host'])) return $path;
		
		$base = explode(DIRECTORY_SEPARATOR, self::path($b['path']));
		$arfl = explode(DIRECTORY_SEPARATOR, self::path($f['path']));
		
		foreach($base as $k => $v){
			if(isset($arfl[$k]) && $base[$k] == $arfl[$k]){
				unset($base[$k], $arfl[$k]);
			}else{
				break;
			}
		}
		
		$file = str_repeat('../', count($base)) . implode('/', $arfl);
		
		$query = [];
		if(isset($f['query'])){
			parse_str($f['query'], $query);
		}
		$query = array_diff_key(array_replace($query, $get), array_flip($exclude));
		$query = self::buildQuery($query, $arg_separator);
		
		return $file . ((empty($query))? '': $arg_query . $query);
	}
	
	/**
	 * Resolve um caminho para um diretorio ou arquivo.
	 *
	 * @param string $path
	 * @param string $divisor
	 * @return string
	 */
	public static function path(string $path, string $divisor = DIRECTORY_SEPARATOR): string
	{
		$resolved = [];
		$path = implode(DIRECTORY_SEPARATOR, explode('\\', $path));
		$path = implode(DIRECTORY_SEPARATOR, explode('/', $path));
		$e = explode(DIRECTORY_SEPARATOR, $path);
		
		foreach($e as $k => $p){
			(($p == '.')? null: (($p == '..' && !empty($resolved) && end($resolved) != '..')? array_pop($resolved): $resolved[] = $p));
		}
		foreach($resolved as $k => $i){
			if((($k == 0 || $k == 1) && isset($resolved[0], $resolved[1]) && (empty($resolved[0]) && empty($resolved[1])))){
				continue;
			}elseif($k == 0 && isset($resolved[0]) && empty($resolved[0])){
				continue;
			}elseif(empty($i)){
				unset($resolved[$k]);
			}
		}
		return implode($divisor, $resolved);
	}
	
	/**
	 * @param array $arrayURL
	 * @param array $merge
	 * @return array
	 */
	public static function parse_get(array $arrayURL, array &$merge = []): array
	{
		$pre = $merge;
		$merge = [];
		$i = 0;
		foreach($arrayURL as $k => $item){
			if(substr_count($item, '=')){
				parse_str(rawurldecode($item), $item);
			}else{
				$item = [$i++ => $item];
			}
			$merge = array_merge_recursive($merge, $item);
		}
		$merge = array_merge_recursive($merge, $pre);
		return $merge;
	}
	
	/**
	 * @param string $uri
	 * @param array  $merge
	 * @return array
	 */
	public static function parse_friendly(string $uri, &$merge = []): array
	{
		$parse = self::basicUrlAnalyzer($uri);
		return self::parse_get($parse['query'], $merge);
	}
	
	public static function getPageUri(): string
	{
		return
			((isset($_SERVER['REQUEST_SCHEME']) && !empty($_SERVER['REQUEST_SCHEME']))? $_SERVER['REQUEST_SCHEME']: 'http') . '://' .
			((isset($_SERVER['SERVER_NAME']) && !empty($_SERVER['SERVER_NAME']))? $_SERVER['SERVER_NAME']: 'localhost') .
			((isset($_SERVER['SERVER_PORT']) && !empty($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] != 80)? ':' . $_SERVER['SERVER_PORT']: '') .
			((isset($_SERVER['REQUEST_URI']) && !empty($_SERVER['REQUEST_URI']))? $_SERVER['REQUEST_URI']: '');
	}
	
	/**
	 * @param string $uri
	 * @return array
	 */
	public static function parseFriendlyUri(string $uri): array
	{
		$parse = array_merge(['scheme' => '', 'host' => '', 'port' => '', 'user' => '', 'pass' => '', 'path' => '', 'query' => '', 'fragment' => ''], parse_url($uri));
		
		$base = self::path(trim(dirname($_SERVER['PHP_SELF'])), '/');
		$path = self::path(trim($parse['path']), '/');
		
		$parse['query'] = $path;
		$parse['path'] = $base;
		
		$parse['query'] = preg_replace('/^' . preg_quote($parse['path'], '/') . '/', '', $parse['query']);
		
		return array_filter($parse);
	}
	
	/**
	 * @param $url
	 * @return array
	 */
	public static function basicUrlAnalyzer($url)
	{
		
		$b['path'] = (trim(dirname($_SERVER['PHP_SELF']), '\\/'));
		$b['path'] .= (!empty($b['path'])? '/': '');
		$b['request'] = (rawurldecode(ltrim($_SERVER['REQUEST_URI'], '\\/')));
		$b['request'] = (explode('?', $b['request'], 2));
		$b['get'] = (isset($b['request'][1])? '?' . $b['request'][1]: '');
		$b['request'] = (!empty($b['request'][0])? $b['request'][0]: '');
		$b['request'] = (!empty($b['request'])? explode('/', $b['request']): []);
		$b['query'] = (array_splice($b['request'], (count((!empty($b['path'])? explode('/', trim($b['path'], '\\/')): [])))));
		$b['request'] = (implode('/', $b['request']));
		$b['request'] .= (!empty($b['request'])? '/': '');
		$b['host'] = (isset($_SERVER['REQUEST_SCHEME'])? $_SERVER['REQUEST_SCHEME']: (isset($_SERVER['HTTPS']) && !empty($_SERVER['HTTPS'])? 'https': 'http')) . '://' . ($_SERVER['HTTP_HOST'] . '/') . $b['request'];
		
		$real = parse_url($b['host']);
		$comp = parse_url($url);
		if(!isset($comp['host'])) $comp = array_merge($comp, [
			'scheme' => $real['scheme'],
			'host'   => $real['host'],
			'path'   => self::path(($real['path'] . ((isset($comp['path']))? $comp['path']: '')), '/'),
		], (isset($comp['query'])? ['query' => $comp['query']]: []));
		
		$r['local'] = ($comp['host'] . '/');
		$r['host'] = ($comp['scheme'] . '://');
		$r['path'] = (trim($real['path'], '\\/'));
		$r['request'] = rawurldecode(ltrim(($comp['path'] . (isset($comp['query'])? '?' . $comp['query']: '')), '\\/'));
		$r['request'] = (explode('?', $r['request'], 2));
		$r['get'] = (isset($r['request'][1])? '?' . $r['request'][1]: '');
		$r['request'] = (!empty($r['request'][0])? $r['request'][0]: '');
		$r['request'] = (!empty($r['request'])? explode('/', $r['request']): []);
		$r['query'] = array_splice($r['request'], (count((!empty($r['path'])? explode('/', $r['path']): []))));
		$r['fragment'] = ((isset($comp['fragment']))? $comp['fragment']: '');
		$r['request'] = implode('/', $r['request']);
		$r['get'] = ((!empty($q = implode('/', $r['query']))? '/': '') . $q . $r['get']);
		$r['endereco'] = $r['host'];
		$r['host'] .= $r['local'];
		$r['real'] = $r['host'] . $r['path'];
		$r['local'] .= (!empty($r['request'])? $r['request']: '');
		$r['endereco'] .= $r['local'] . (!empty($r['request'])? '/': '');
		$r['full'] = $r['real'] . $r['get'] . ((empty($r['fragment']))? '': '#' . $r['fragment']);
		
		return $r;
	}
	
	public static function parseHeaderValue(string $headerValue): array
	{
		$trim = fn($t) => trim(trim($t), "\n\r\t\v\0\"' ");
		$itens = explode(',', $headerValue);
		$resultado = [];
		
		foreach($itens as $item){
			$partes = explode(';', $item);
			$atributos = [];
			
			foreach($partes as $parte){
				$atributo = explode('=', $parte, 2);
				if(count($atributo) == 2){
					$atributos[$trim($atributo[0])] = $trim($atributo[1]);
				}elseif(count($atributo) == 1){
					$atributos[] = $trim($atributo[0]);
				}
			}
			
			if(!empty($atributos)) $resultado[] = $atributos;
		}
		
		return $resultado;
	}
	
}